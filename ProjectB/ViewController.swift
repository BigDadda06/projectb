//
//  ViewController.swift
//  ProjectA
//
//  Created by Malcolm Chalmers on 23/6/18.
//  Copyright © 2018 Malcolm Chalmers. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var shapePicker: UISegmentedControl!
    @IBOutlet weak var colorPicker: UIPickerView!
    
    let df = DateFormatter()
    let colors = ["black","brown","red","green","yellow","white/grey"]
    var selectedColor = ""
    
    override func viewDidLoad() {
        let today = Date()
        datePicker.date = today
    }


    // MARK: UI Wiring
    
    @IBAction func doneButton(_ sender: UIButton) {
        
        switch shapePicker.selectedSegmentIndex  {
        case 0:
            print ("Formed")
        case 1:
            print ("Unformed")
        default:
            print (String(shapePicker.selectedSegmentIndex))
        }
        
        df.dateStyle = .long
        df.timeStyle = .long
        print (df.string(from: datePicker.date))
        
        print (selectedColor)
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return colors[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedColor = colors[row]
    }
    
    @IBAction func shapePicked(_ sender: UISegmentedControl) {
    }
    
}

